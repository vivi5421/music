#!/usr/bin/python
import adafruit_mpr121
import board
import busio
import os
import pygame as pg
import sys
import time

from config import NB_PINS, PINS
 
freq = 44100    # audio CD quality
bitsize = -16   # unsigned 16 bit
channels = 2    # 1 is mono, 2 is stereo
buffer = 2048   # number of samples (experiment to get right sound)
pg.mixer.init(freq, bitsize, channels, buffer)
pg.mixer.music.set_volume(1.0)

i2c = busio.I2C(board.SCL, board.SDA)
mpr121 = adafruit_mpr121.MPR121(i2c)
 
def play(music_file):
    '''
    stream music with mixer.music module in blocking manner
    this will stream the sound from disk while playing
    '''
    clock = pg.time.Clock()
    try:
        pg.mixer.music.load(music_file)
        print("Music file {} loaded!".format(music_file))
    except pg.error:
        print("File {} not found! {}".format(music_file, pg.get_error()))
        return

    pg.mixer.music.play()
#    while pg.mixer.music.get_busy():
#       clock.tick(30)

if __name__ == '__main__':
    while True:
        for i in range(NB_PINS):
            if mpr121[i].value:
                print("Pin {} touched!".format(i))
                play(os.path.join(PINS, str(i)))
