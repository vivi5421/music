import bluetoothctl
import glob
import os
from config import SOUNDS, PINS, NB_PINS, PORT

from flask import Flask, render_template, request
from werkzeug.utils import secure_filename

app = Flask(__name__)
btctl = bluetoothctl.Bluetoothctl()

def cmd(command):
    print(command)
    os.system(command)

def tracks():
    return os.listdir(SOUNDS)

def pins():
    return [os.readlink(os.path.join(PINS, str(i))) for i in range(NB_PINS)]

def bluetooths():
    return btctl.get_available_devices()        

        
@app.route('/', methods=['GET', 'POST'])
def pisoundweb():
    if request.method == 'POST':
        for i in range(NB_PINS):
            cmd('ln -sf {}/{} {}/{}'.format(SOUNDS, request.form.get(str(i)), PINS, i))
    _tracks = tracks()
    _pins = pins()
    print('tracks={}'.format(_tracks))
    print('pins={}'.format(_pins))
    return render_template('uploader.html', tracks=_tracks, pins=_pins, bluetooths=bluetooths())


@app.route('/upload', methods=['POST'])
def upload():
    file = request.files['file']
    if file:
        filename = secure_filename(file.filename)
        file.save(os.path.join(SOUNDS, filename))
        return render_template('uploader.html', tracks=tracks(), pins=pins(), bluetooths=bluetooths())



@app.route('/reset', methods=['GET'])
def reset():
    cmd('sudo systemctl restart pisound')
    return render_template('uploader.html', tracks=tracks(), pins=pins(), bluetooths=bluetooths())



@app.route('/connect', methods=['POST'])
def connect():
    mac = request.form.get('mac')
    os.system('echo "trust {}" | bluetoothctl'.format(mac))
    os.system('echo "connect {}" | bluetoothctl'.format(mac))
    return render_template('uploader.html', tracks=tracks(), pins=pins(), bluetooths=bluetooths())


if __name__ == '__main__':
    btctl.start_scan()
    app.run(debug=True, host='0.0.0.0', port=PORT)