import os

PORT = 5003

STATIC = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'static')
SOUNDS = os.path.join(STATIC, 'sounds')
PINS = os.path.join(STATIC, 'pins')
NB_PINS = 12